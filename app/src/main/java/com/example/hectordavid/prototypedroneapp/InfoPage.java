package com.example.hectordavid.prototypedroneapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class InfoPage extends AppCompatActivity {

    private TextView IPTextView;
    private TextView serverPortTextView;
    private TextView creditTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_page);

        //update action bar title
        setTitle(getString(R.string.infopage_title));

        //display target port
        serverPortTextView = (TextView) findViewById(R.id.serverPortTextView);
        serverPortTextView.setText("Target Port: " + getString(R.string.server_port));

        //display target IP
        IPTextView = (TextView) findViewById(R.id.IPTextView);
        IPTextView.setText("Drone's IP:   " + getString(R.string.target_ip));

        //set credits
        creditTextView = (TextView) findViewById(R.id.creditTextView);
        creditTextView.setText(getString(R.string.credits));


    }//end onCreate
}//end InfoPage
