package com.example.hectordavid.prototypedroneapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    

    private Button startButton;
    private Button connectButton;
    private EditText ipEditTextData;
    private TextView IPTextView;
    private TextView serverPortTextView;
    private TextView messageTextView;
    private TextView receiveTextView;
    private TextView statusTextView;
    boolean CONNECTF = false; //connection flag to see if app is connected

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //need this to bypass Network Exception errors
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //display target port
        serverPortTextView = (TextView) findViewById(R.id.serverPortTextView);
        serverPortTextView.setText("Port: " + getString(R.string.server_port));

        //display target IP
        IPTextView = (TextView) findViewById(R.id.IPTextView);
        IPTextView.setText("IP: " + getString(R.string.target_ip));

        //listener for connect button
        connectButton = (Button) findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                   try {
                      connectToUDP();
                      if (CONNECTF)
                          Toast.makeText(getApplicationContext(), getString(R.string.connected), Toast.LENGTH_LONG).show();
                      else
                          Toast.makeText(getApplicationContext(), getString(R.string.connnection_fail), Toast.LENGTH_LONG).show();
                   }catch (Exception e)
                   {
                       StringWriter errors = new StringWriter();
                       e.printStackTrace(new PrintWriter(errors));
                       String hier2 =  errors.toString();
                       Toast.makeText(getApplicationContext(), "Exception :" + hier2, Toast.LENGTH_LONG).show();
                   }
                   catch (Throwable th)
                   {
                       StringWriter errors = new StringWriter();
                       th.printStackTrace(new PrintWriter(errors));
                       String hier3 =  errors.toString();
                       Toast.makeText(getApplicationContext(), "Throwable :" + hier3, Toast.LENGTH_LONG).show();
                   }
            }
        });//end connectButton listener

        //listener for start flight button
        startButton = (Button) findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    connectToUDP();
                    if(CONNECTF)
                    sendToUDP(getString(R.string.message));
                    else {
                        Toast.makeText(getApplicationContext(), getString(R.string.start_error), Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e)
                {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));
                    String hier2 =  errors.toString();
                    Toast.makeText(getApplicationContext(), "Exception :" + hier2, Toast.LENGTH_LONG).show();
                }
                catch (Throwable th)
                {
                    StringWriter errors = new StringWriter();
                    th.printStackTrace(new PrintWriter(errors));
                    String hier3 =  errors.toString();
                    Toast.makeText(getApplicationContext(), "Throwable :" + hier3, Toast.LENGTH_LONG).show();
                }
            }
        });//end startButton listener

    }//end onCreate

    //send a string over UDP
    void sendToUDP(String message) throws SocketException, IOException, Exception, Throwable
    {
        receiveTextView = (TextView) findViewById(R.id.receiveTextView);
        messageTextView = (TextView) findViewById(R.id.messageTextView);
        messageTextView.setText(message);

        DatagramSocket clientSocket = new DatagramSocket();

        InetAddress ipaddr = InetAddress.getByName(getString(R.string.target_ip));

        byte[] sendData    = new byte[1024];
        byte[] receiveData = new byte[1024];

        String send_msg = message;
        sendData = send_msg.getBytes();

        //sending the message to target
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipaddr, Integer.parseInt(getString(R.string.server_port)));
        clientSocket.send(sendPacket);

        //receiving a package from the same target
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);
        String receive_msg = new String(receivePacket.getData(), 0, receivePacket.getLength());
        receiveTextView.setText(receive_msg);

        if (receive_msg.equalsIgnoreCase("success"))
        {
            Toast.makeText(getApplicationContext(), getString(R.string.successful_start), Toast.LENGTH_LONG).show();
        }


        clientSocket.close();
    }//end sendToUDP

    //check if connection can be established with target
    void connectToUDP() throws SocketException, IOException, Exception, Throwable
    {
        statusTextView = (TextView) findViewById(R.id.statusTextView);
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress ipaddr = InetAddress.getByName(getString(R.string.target_ip));

        if (ipaddr.isReachable(100)) //timeout time in milliseconds
        {
            CONNECTF = true;
            statusTextView.setText(getString(R.string.connected));
        }else {
            CONNECTF = false;
            statusTextView.setText(getString(R.string.disconnected));
        }
    }//end connectToUDP

}//end MainActivity
