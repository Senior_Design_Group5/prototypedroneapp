package com.example.hectordavid.prototypedroneapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    private Button startButton;
    private EditText ipEditTextData;
    private TextView IPTextView;
    private TextView serverPortTextView;
    private TextView messageTextView;




    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //need this to bypass Network Exception errors
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //display target port
        serverPortTextView = (TextView) findViewById(R.id.serverPortTextView);
        serverPortTextView.setText("Port: " + getString(R.string.server_port));

        //display target IP
        IPTextView = (TextView) findViewById(R.id.IPTextView);
        IPTextView.setText("IP: " + getString(R.string.target_ip));

        startButton = (Button) findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    sendToUDP("Hector is cool");
                }catch (Exception e)
                {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));
                    String hier2 =  errors.toString();
                    Toast.makeText(getApplicationContext(), "Exception :" + hier2, Toast.LENGTH_LONG).show();
                }
                catch (Throwable th)
                {
                    StringWriter errors = new StringWriter();
                    th.printStackTrace(new PrintWriter(errors));
                    String hier3 =  errors.toString();
                    Toast.makeText(getApplicationContext(), "Throwable :" + hier3, Toast.LENGTH_LONG).show();
                }

            }
        });



    }//end onCreate

    //send a string over UDP
    void sendToUDP(String message) throws SocketException, IOException, Exception, Throwable
    {
        messageTextView = (TextView) findViewById(R.id.messageTextView);
        messageTextView.setText(message);

        DatagramSocket clientSocket = new DatagramSocket();

        InetAddress ipaddr = InetAddress.getByName("192.168.43.195");

        byte[] sendData    = new byte[1024];
        byte[] receiveData = new byte[1024];

        String sentence = message;
        sendData = sentence.getBytes();

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipaddr, 10002);

        clientSocket.send(sendPacket);
        clientSocket.close();
    }//end sendToUDP

}//end MainActivity
